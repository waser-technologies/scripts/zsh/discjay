#!/usr/bin/env zsh

function show_help {
    echo "Usage:"
    echo "discjay.zsh -i INPUT_CSV_FILE -o OUPUT_DIR [-h]"
    echo
    echo "-i INPUT_CSV_FILE\tCSV file where the songs url are stored"
    echo "-o OUPUT_DIR\tDestiation dir to download the songs"
}

function download_wav_from_url {
    video_url=$1
    song_dest=$2
    format='wav'
    echo
    echo "Downloading video with ID ${video_url}"
    yt-dlp -x --audio-format $format "${video_url}" -o ${output_dest}/${song_dest}.${format}
    echo "Download complete"
    echo
}

if [ ! $1 ];then
    echo "No input file."
    show_help
    exit 1
elif [ $1 = '-h' ];then
    show_help
    exit 0
elif [ $1 = '-i' ];then
    input_csv_file=$2
fi

if [ $3 = '-o' ];then
    output_dest=$4
fi

n=`wc -l ${input_csv_file}`
i=0

echo "Downloading ${n} songs ..."

# This is a handy way to make the user wait nicely
. <(curl -sLo- "https://git.io/progressbar")

bar::start

while IFS=',' read -r song_name artist_name download_video_url  || [ -n "${LINE}" ]; do
    i=$((i+1))
    download_wav_from_url $download_video_url $song_name
done < <(tail -n +2 $input_csv_file)

bar::stop

echo "Download complete"